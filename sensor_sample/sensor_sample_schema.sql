CREATE SCHEMA sensordata;

set search_path=sensordata;

CREATE TABLE sensordata.station_info
(
STATION_ID VARCHAR,
TYPE CHAR,
LONGITUDE FLOAT,
LATITUDE FLOAT,
PRIMARY KEY (STATION_ID)
);


CREATE TABLE sensordata.qv_iode
(
iode_val INTEGER,
reading VARCHAR,
PRIMARY KEY (iode_val)
);

CREATE TABLE sensordata.main
(
id VARCHAR,
SAMPLE_ID INTEGER,
CRUISE VARCHAR(4),STATION VARCHAR,TIMESTAMP DATE NOT NULL,
CTDPRS float,CTDPRS_QV integer,CTDTMP float,CTDTMP_QV integer,CTDSAL float,CTDSAL_QV integer,CTDOXY float,CTDOXY_QV integer,Fluorescence float,Fluorescence_qv integer,Fluorescence_raw float,flourecence_raw_qv integer,Transmissometer_Beam_Attenuation float,Transmissometer_Beam_Attenuation_qv integer,
Transmissometer_Raw_Data integer,Transmissometer_Raw_Data_qv integer,Turbidity_Raw_Data float,Turbidity_Raw_Data_qv integer,PAR_Raw_Data integer,PAR_Raw_Data_qv integer,
PRIMARY KEY (id)
);
