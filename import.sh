if psql --list | grep oceandb >/dev/null
then
    echo "Data will be imported into the existing oceandb database."
    echo -n " Continue? [y/N]: "
    read ANSWER
    case $ANSWER in
        y*|Y*) echo "OK, starting import." ;;
            *) echo "Cancelled, oceandb has not been changed."
            exit ;;
    esac
else
    echo "Creating oceandb ..."
    createdb oceandb
    echo "Loading the sampledata schema ..."
    psql -d oceandb -f `pwd`/discrete_sample/discrete_sample_schema.sql
    psql -d oceandb -f `pwd`/sensor_sample/sensor_sample_schema.sql
fi


echo "Importing Discrete Data oceandb"

echo "loading cruise_info"
cat `pwd`/discrete_sample/discrete_sample_cruise_info.csv | psql -d oceandb -c "COPY sampledata.cruise_info from stdin DELIMITER ',' CSV HEADER;"

echo "loading main table"
cat `pwd`/discrete_sample/discrete_sample_main.csv | psql -d oceandb -c "COPY sampledata.main from stdin DELIMITER ',' CSV HEADER;"

echo "loading station_info"
cat `pwd`/discrete_sample/discrete_sample_station_info.csv | psql -d oceandb -c "COPY sampledata.station_info from stdin DELIMITER ',' CSV HEADER;"

echo "loading iode_val"
cat `pwd`/discrete_sample/discrete_sample_iod_val.csv | psql -d oceandb -c "COPY sampledata.qv_iode from stdin DELIMITER ',' CSV HEADER;"



echo "Importing Sensor Data to oceandb"

echo "loading sensor data main table"
cat `pwd`/sensor_sample/vg_both_sensor_main.csv | psql -d oceandb -c "COPY sensordata.main from stdin DELIMITER ',' CSV HEADER;"

echo "loading station_info"
cat `pwd`/sensor_sample/vg_both_sensor_station_info.csv | psql -d oceandb -c "COPY sensordata.station_info from stdin DELIMITER ',' CSV HEADER;"

echo "loading iode_val"
cat `pwd`/sensor_sample/sensor_sample_iod_val.csv | psql -d oceandb -c "COPY sensordata.qv_iode from stdin DELIMITER ',' CSV HEADER;"
