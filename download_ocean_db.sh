echo -n Username:
read USERNAME
echo

echo -n Password: 
read -s PASSWORD
echo

echo $USERNAME
echo $PASSWORD

mkdir `pwd`/discrete_sample
mkdir `pwd`/sensor_sample

echo "downloading import script..."

curl -u $USERNAME:$PASSWORD -o `pwd`/import.sh https://txe1-portal.mit.edu/gridsan/groups/istcdata/datasets/ocean_metagenome/pipeline/Step0_Raw/metadata/import.sh

echo "downloading discrete samples..."

curl -u $USERNAME:$PASSWORD -o `pwd`/discrete_sample/discrete_sample_cruise_info.csv https://txe1-portal.mit.edu/gridsan/groups/istcdata/datasets/ocean_metagenome/pipeline/Step0_Raw/metadata/discrete_sample/discrete_sample_cruise_info.csv

curl -u $USERNAME:$PASSWORD -o `pwd`/discrete_sample/discrete_sample_iod_val.csv https://txe1-portal.mit.edu/gridsan/groups/istcdata/datasets/ocean_metagenome/pipeline/Step0_Raw/metadata/discrete_sample/discrete_sample_iod_val.csv

curl -u $USERNAME:$PASSWORD -o `pwd`/discrete_sample/discrete_sample_main.csv https://txe1-portal.mit.edu/gridsan/groups/istcdata/datasets/ocean_metagenome/pipeline/Step0_Raw/metadata/discrete_sample/discrete_sample_main.csv

curl -u $USERNAME:$PASSWORD -o `pwd`/discrete_sample/discrete_sample_schema.sql https://txe1-portal.mit.edu/gridsan/groups/istcdata/datasets/ocean_metagenome/pipeline/Step0_Raw/metadata/discrete_sample/discrete_sample_schema.sql

curl -u $USERNAME:$PASSWORD -o `pwd`/discrete_sample/discrete_sample_station_info.csv https://txe1-portal.mit.edu/gridsan/groups/istcdata/datasets/ocean_metagenome/pipeline/Step0_Raw/metadata/discrete_sample/discrete_sample_station_info.csv

echo "downloading sensor samples..."

curl -u $USERNAME:$PASSWORD -o `pwd`/sensor_sample/sensor_sample_iod_val.csv https://txe1-portal.mit.edu/gridsan/groups/istcdata/datasets/ocean_metagenome/pipeline/Step0_Raw/metadata/sensor_sample/sensor_sample_iod_val.csv

curl -u $USERNAME:$PASSWORD -o `pwd`/sensor_sample/sensor_sample_schema.sql https://txe1-portal.mit.edu/gridsan/groups/istcdata/datasets/ocean_metagenome/pipeline/Step0_Raw/metadata/sensor_sample/sensor_sample_schema.sql

curl -u $USERNAME:$PASSWORD -o `pwd`/sensor_sample/vg_both_sensor_main.csv https://txe1-portal.mit.edu/gridsan/groups/istcdata/datasets/ocean_metagenome/pipeline/Step0_Raw/metadata/sensor_sample/vg_both_sensor_main.csv

curl -u $USERNAME:$PASSWORD -o `pwd`/sensor_sample/vg_both_sensor_station_info.csv https://txe1-portal.mit.edu/gridsan/groups/istcdata/datasets/ocean_metagenome/pipeline/Step0_Raw/metadata/sensor_sample/vg_both_sensor_station_info.csv


